from django.forms import ModelForm
from django import forms
from .models import *

class ProductsAdd(ModelForm):
    class Meta:
        model = Produk
        fields = '__all__'

class TokoAdd(ModelForm):
    class Meta:
        model = Toko
        fields = '__all__'