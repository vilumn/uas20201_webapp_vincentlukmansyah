from django.db import models
from django.contrib.auth.models import User

class Toko(models.Model):
    nama_toko = models.CharField(default='', max_length=256)
    alamat = models.CharField(default='', max_length=256)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return '{}'.format(self.nama_toko)

class Produk(models.Model):
    nama = models.CharField(default='', max_length=256)
    harga = models.IntegerField()
    image = models.FileField(blank = True, null = True)
    keterangan = models.CharField(default='', max_length=256)
    toko = models.ForeignKey(Toko, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return '{}'.format(self.nama)

