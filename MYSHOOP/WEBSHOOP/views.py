from django.shortcuts import render
from WEBSHOOP import models, forms
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.models import User

# Create your views here.
def products(request):
    product = models.Produk.objects.all()
    if request.method == "POST":
        keyword = request.POST.get('keyword', '')
        result = models.Produk.objects.filter(toko__user__username__icontains= keyword)
        if keyword != '':
            bagian = 'product'
            context = {
                'product': result,
                'bagian': bagian,
            }
            return render(request, 'products.html', context)

    bagian = 'product'
    nama = 'Produk'
    context = {
        'product': product,
        'bagian': bagian,
    }
    return render(request, 'products.html', context)

def toko(request):
    nama_toko = models.Toko.objects.all()
    if request.method == "POST":
        keyword = request.POST.get('keyword', '')
        result = models.Toko.objects.filter(user__username__icontains= keyword)
        if keyword != '':
            bagian = 'toko'
            context = {
                'toko': result,
                'bagian': bagian,
            }
            return render(request, 'toko.html', context)

    bagian = 'toko'
    context = {
        'toko': nama_toko,
        'bagian': bagian,
    }
    return render(request, 'toko.html', context)

class CreateProducts(CreateView):
    template_name = "products_add.html"
    form_class = forms.ProductsAdd

    def get_success_url(self):
        return reverse_lazy('products')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kategori"] = 'Product'
        return context

class UpdateProducts(UpdateView):
    template_name = "products_add.html"
    form_class = forms.ProductsAdd
    model = models.Produk

    def get_success_url(self):
        return reverse_lazy('products')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kategori"] = 'Product'
        return context

class DeleteProducts(DeleteView):
    template_name = "products_delete.html"
    form_class = forms.ProductsAdd
    model = models.Produk

    def get_success_url(self):
        return reverse_lazy('products')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kategori"] = 'Product'
        return context

class CreateToko(CreateView):
    template_name = "products_add.html"
    form_class = forms.TokoAdd

    def get_success_url(self):
        return reverse_lazy('toko')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kategori"] = 'Toko'
        return context

class UpdateToko(UpdateView):
    template_name = "products_add.html"
    form_class = forms.TokoAdd
    model = models.Toko

    def get_success_url(self):
        return reverse_lazy('toko')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kategori"] = 'Toko'
        return context

class DeleteToko(DeleteView):
    template_name = "products_delete.html"
    form_class = forms.TokoAdd
    model = models.Toko

    def get_success_url(self):
        return reverse_lazy('toko')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["kategori"] = 'Toko'
        return context