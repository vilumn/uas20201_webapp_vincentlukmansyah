from django.contrib import admin
from django.urls import path
from WEBSHOOP import views

urlpatterns = [
    path('admin/', admin.site.urls),

    #Product
    path('products/', views.products, name='products'),
    path('products/create', views.CreateProducts.as_view(), name='products_add'),
    path('products/update/<int:pk>', views.UpdateProducts.as_view(), name='products_update'),
    path('products/delete/<int:pk>', views.DeleteProducts.as_view(), name='products_delete'),

    #Toko
    path('toko/', views.toko, name='toko'),
    path('toko/create', views.CreateToko.as_view(), name='toko_add'),
    path('toko/update/<int:pk>', views.UpdateToko.as_view(), name='toko_update'),
    path('toko/delete/<int:pk>', views.DeleteToko.as_view(), name='toko_delete'),
]
